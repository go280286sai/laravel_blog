import {defineConfig} from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/app.js',
                'resources/css/bootstrap.min.css',
                'resources/css/font-awesome.min.css',
                'resources/css/ionicons.min.css',
                'resources/css/AdminLTE.min.css',
                'resources/css/_all-skins.min.css',
                'resources/js/bootstrap.min.js',
                'resources/js/jquery.slimscroll.min.js',
                'resources/js/fastclick.js',
                'resources/js/app.min.js',
                'resources/js/demo.js',
                'resources/js/jquery.dataTables.min.js',
                'resources/js/dataTables.bootstrap.min.js',
                'resources/js/dataTables.bootstrap.css',
                'resources/css/_all.css',
                'resources/css/datepicker3.css',
                'resources/css/select2.min.css',
                'resources/js/select2.full.min.js',
                'resources/js/bootstrap-datepicker.js',
                'resources/js/icheck.min.js'
            ],
            refresh: true,
        }),
    ],
});
